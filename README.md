# ss2024-weyls-law

These are notes for a talk I am giving in a seminar in Bielefeld on trace formulae (organized by Sarah Meier).
A compiled version can be found [here](https://makoba.gitlab.io/ss2024-weyls-law/weyls-law.pdf).