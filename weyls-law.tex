\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{The trace formula for $\SL(2, \RR)$}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    
    These are notes for a talk I am giving in a seminar on \emph{trace formulae}, organized by Sarah Meier and taking place in the summer term 2024 in Bielefeld.
    The main reference for this talk is \cite[Section 4]{waterhouse}.

    Use these notes at your own risk, they probably contain mistakes (feel free to tell me about these mistakes)!

    \section{Notation}

    We use the following notation.

    \begin{itemize}
        \item
        $G = \SL(2, \RR)$ denotes the (real) Lie group of $(2 \times 2)$-matrices over the real numbers of determinant $1$.

        \item
        We write $\frakg = \Lie(G)$ for the Lie algebra of $G$.
        It spanned (over $\RR$) by the elements $R = \begin{psmallmatrix} 0 & 1 \\ 0 & 0 \end{psmallmatrix}$, $L = \begin{psmallmatrix} 0 & 0 \\ 1 & 0 \end{psmallmatrix}$ and $H = \begin{psmallmatrix} 1 & 0 \\ 0 & -1 \end{psmallmatrix}$.

        \item
        We set
        \begin{align*}
            A &\coloneqq \set[\Bigg]{a(u) = \begin{pmatrix} e^{u/2} & 0 \\ 0 & e^{u/2} \end{pmatrix}}{u \in \RR},
            \\
            N &\coloneqq \set[\Bigg]{n(x) = \begin{pmatrix} 1 & x \\ 0 & 1 \end{pmatrix}}{x \in \RR}, \\
            K &\coloneqq \set[\Bigg]{k(\theta) = \begin{pmatrix} \cos \theta & \sin \theta \\ - \sin \theta & \cos \theta \end{pmatrix}}{\theta \in \RR} = \SO(2) \subseteq G \, .
        \end{align*}
        The statement of Iwasawa decomposition is that multiplication induces a diffeomorphism $A \times N \times K \cong G$.
        Using this we can normalize the Haar measure $\dd g$ on $G$ by $\dd g = (2 \pi)^{-1} \dd u \, \dd x \, \dd \theta$.
        We also normalize the Haar measure $\dd k$ on $K$ by $\dd k = (2 \pi)^{-1} \, \dd \theta$.

        We also write $B \coloneqq \pm A N \subseteq G$ for the Borel subgroup.

        \item
        Write $W = N_G(A)/\pm A$ for the Weyl group of $(G, \pm A)$.
        It is a cyclic group of order $2$; representatives of the two elements in $W$ are given by $I = \begin{psmallmatrix} 1 & 0 \\ 0 & 1 \end{psmallmatrix}$ and $w = \begin{psmallmatrix} 0 & -1 \\ 1 & 0 \end{psmallmatrix}$.

        \item
        $\calH$ denotes the complex upper half plane.
        A typical element in $\calH$ is denoted by $z = x + iy$.
        We endow $\calH$ with the metric
        \[
            \dd s^2 \coloneqq \frac{1}{y^2} \roundbr[\big]{\dd x^2 + \dd y^2} \, .
        \]
        Consequently we obtain a measure $\dd z = y^{-2} \dd x \, \dd y$ and a Laplacian $\Delta = y^2 (\partial_x^2 + \partial_y^2)$.
        
        \item
        $G$ acts on $\calH$ by Möbius transformations and this action is transitive.
        In fact this action factors through the quotient $G/\curlybr{\pm I} = \PGL(2, \RR)^+$ and induces an isomorphism $\PGL(2, \RR)^+ \cong \Aut(\calH)$.
        The stabilizer in $G$ of the base point $i \in \calH$ is precisely $K$.
        We thus obtain a diffeomorphism $G/K \cong \calH$.

        \item
        We fix a discrete cocompact subgroup $\Gamma \subseteq G$ and assume that $-I \in \Gamma$ and that $\Gamma$ is hyperbolic, i.e.\ acts freely on $\calH$.
        The set of such subgroups $\Gamma$ is in bijection with the set of isomorphism classes of pairs $(X, \pi)$ consisting of a compact connected Riemann surface $X$ (of genus $\geq 2$) and a covering map $\pi \colon \calH \to X$.
    \end{itemize}

    Our goal is now to give somewhat explicit descriptions of both sides of the trace formula for $(G, \Gamma)$ and a test function $f \in C_c^{\infty}(G // K)$.

    \section{\texorpdfstring{Bi-$K$-invariant functions}{Bi-K-invariant functions}}

    \begin{theorem}
        We have an isomorphism of algebras $C_c^{\infty}(G//K) \to C_c^{\infty}(\RR)^{\even}$ that is given by
        \[
            f \mapsto \roundbr[\Bigg]{g_f \colon u \mapsto \int_{- \infty}^{\infty} f \roundbr[\Big]{\begin{pmatrix} e^{u/2} & x \\ 0 & e^{-u/2} \end{pmatrix}} \, \dd x} \, .
        \]
    \end{theorem}

    In the following we write
    \[
        \widehat{g}(v) \coloneqq \int_{- \infty}^{\infty} g(u) e^{iuv} \, \dd u
    \]
    for the Fourier transform of some $g \in C_c^{\infty}(\RR)$.

    \section{Automorphic forms}

    \begin{definition}
        We write $\calA(\Gamma \backslash \calH) \coloneqq C^{\infty}(\Gamma \backslash \calH)$ for the vector space of smooth ($\CC$-valued) functions on $\Gamma \backslash \calH$ and equip it with an inner product by setting
        \[
            \anglebr[\big]{\varphi_1, \varphi_2} \coloneqq \int_{\Gamma \backslash \calH} \varphi_1(z) \cdot \overline{\varphi_2(z)} \, \dd z \, .
        \]
        Note that an application of Stokes' theorem yields that the Laplace operator $\Delta \colon \calA(\Gamma \backslash \calH) \to \calA(\Gamma \backslash \calH)$ is self-adjoint and negative semi-definite.

        An \emph{automorphic form} is a function $\varphi \in \calA(\Gamma \backslash \calH)$ that satisfies the equation
        \[
            \Delta \varphi + \lambda \varphi = 0
        \]
        for some $\lambda \in \RR_{\geq 0}$.
    \end{definition}

    \begin{theorem}
        Let $H \subseteq L^2(\Gamma \backslash G)$ be a closed irreducible (admissible) $G$-representation.

        Then the $\CC$-subspace $H^K \subseteq H$ of $K$-invariants is at most one-dimensional.
        If $0 \neq \varphi \in H^K$ then we actually have $\varphi \in C^{\infty}(\Gamma \backslash G / K) \cong \calA(\Gamma \backslash \calH)$ and $\Delta \varphi + \lambda \varphi = 0$ for some $\lambda \in \RR_{\geq 0}$ depending only on the isomorphism class of $H$.
    \end{theorem}

    \section{\texorpdfstring{$(\frakg, K)$-modules}{(g, K)-modules}}

    \begin{definition}
        Let $\pi$ be a representation of $G$ on a Hilbert space $H$.
        A vector $v \in H$ is called $C^1$ if the derivative
        \[
            \pi(X) v \coloneqq \restr{\frac{\dd}{\dd t} \pi(\exp(tX)) v}{t = 0}
        \]
        exists for all $X \in \frakg$.
        For $k \geq 2$ we now inductively define $v \in H$ to be $C^k$ if $v$ is $C^1$ and $\pi(X) v \in C^{k - 1}$ for all $X \in \frakg$.
        Finally we say that $v \in H$ is $C^{\infty}$ or \emph{smooth} if it is $C^k$ for all $k \geq 1$.
        This is equivalent to saying that the map $G \to H$ defined by $g \mapsto \pi(g) v$ is smooth.

        We write $H^{\infty} \subseteq H$ for the subspace of smooth vectors.
        This is naturally a $\frakg$-representation.
    \end{definition}

    \begin{lemma}
        We have $L^2(\Gamma \backslash G)^{\infty} = C^{\infty}(\Gamma \backslash G)$.
        Given $\varphi \in C^{\infty}(\Gamma \backslash G)^K = C^{\infty}(\Gamma \backslash \calH)$ we have $\Delta \varphi = -C \varphi$, where $C = - \frac{1}{4} (H^2 + 2 RL + 2 LR) \in U(\frakg)$ is the \emph{Casimir element}.
        This element is a polynomial generator of the center $Z(\frakg)$ of the enveloping algebra $U(\frakg)$.
    \end{lemma}

    \section{Spherical representations}

    Given a complex number $s \in \CC$ let us write
    \[
        \chi_s \colon B \to \CC^{\times }, \qquad \begin{pmatrix} a & b \\ 0 & a^{-1} \end{pmatrix} \mapsto \abs{a}^s \, .
    \]
    We also write $\delta_B \colon B \to \CC^{\times}$ for the modular character of $B$.
    Note that we then have $\delta_B = \chi_2$.

    \begin{definition}
        For $s \in \CC$ we define
        \[
            H_s \coloneqq \set[\Big]{\varphi \colon G \to \CC}{\text{$\varphi(b g) = \chi_s(b) \delta_B(b)^{1/2} \varphi(g)$ for all $b \in B$, $g \in G$ and $\restr{\varphi}{K} \in L^2(K)$}} \, .
        \]
        We let $G$ act on $H_s$ by right translation and topologize $H_s$ via the isomorphism $H_s \cong L^2(K)^{\ZZ/2}$.
    \end{definition}

    \begin{proposition}
        $H_s$ is an admissible $G$-representations; it is irreducible unless $s \in 2 \ZZ + 1$ and we have $\dim H_s^K = 1$.
        Moreover:
        \begin{itemize}
            \item
            Suppose that $s \in i \RR$.
            Then the usual scalar product $\anglebr{\varphi_1, \varphi_2} \coloneqq \int_K \varphi_1(k) \overline{\varphi_2(k)} \, \dd k$ on $H_s$ makes it into a unitary $G$-representation.

            \item
            Now suppose that $s \in \RR$.
            Then the expression
            \[
                \roundbr[\big]{M(s) \varphi}(g) \coloneqq \int_{- \infty}^{\infty} \varphi \roundbr[\big]{w n(x) g} \, \dd x
            \]
            defines a map of $G$-representations $M(s) \colon H_s \to H_{-s}$.
            For $s \in (-1, 1)$ we obtain a scalar product
            \[
                \anglebr{\varphi_1, \varphi_2} \coloneqq \int_K \varphi_1(k) \overline{\roundbr[\big]{M(s) \varphi_2}(k)} \, \dd k
            \]
            on $H_s$ that makes it into a unitary $G$-representation.
        \end{itemize}
    \end{proposition}

    \begin{definition}
        For $s \in i \RR \cup (-1, 1)$ we write $\pi_s$ for the unitary irreducible $G$-representation $H_s$.
        For $s = \pm 1$ we write $\pi_s$ for the trivial $G$-representation.
    \end{definition}

    \begin{theorem} \label{thm:spherical-reps}
        We have a bijection
        \[
            \roundbr[\big]{i\RR \cup [-1, 1]}/(s \sim -s) \to \curlybr[\Big]{\text{irreducible unitary $G$-representations $\pi$ such that $\pi^K \neq 0$}}, \qquad s \mapsto \pi_s \, .
        \]
        Let $s \in i\RR \cup [-1, 1]$.
        Then $C$ acts on $\pi_s^K$ by $\frac{1 - s^2}{4} \varphi$ and for $f \in C_c^{\infty}(G // K)$ we have
        \[
            \tr \pi_s(f) = \int_{- \infty}^{\infty} \int_{- \infty}^{\infty} f \roundbr[\big]{a(u) n(x)} e^{(1 + s)u/2} \, \dd u \, \dd x = \widehat{g_f}\roundbr[\big]{s/2i} \, .
        \]
    \end{theorem}

    \begin{proof}
        We explain how one computes the action of the Casimir element (for $s \neq \pm 1$).
        The space of $K$-invariants $\pi_s^K$ is spanned by the smooth function $\varphi \colon G \to \CC$ defined by $\varphi(a(u) n(x) k(\theta)) \coloneqq e^{(1 + s)u/2}$.
        Viewing $\varphi$ as a smooth function on $\calH$ we have $\varphi(x + iy) = y^{(s + 1)/2}$.
        We can then use the identity $C \varphi = - \Delta \varphi$ to conclude.

        The trace $\tr \pi_s(f)$ is given by the scalar by which $\pi_s(f)$ acts on $\pi_s^K$ and this then yields the above formula.
    \end{proof}

    \begin{corollary}
        In the orthogonal decomposition
        \[
            L^2(\Gamma \backslash G) \cong \bigoplus_{\pi \in \widehat{G}} m_{\pi} \cdot \pi
        \]
        we have $m_{\pi_s} = \dim \calA(\Gamma \backslash \calH)[\Delta + \lambda_s]$, where we have set $\lambda_s \coloneqq \frac{1 - s^2}{4}$.
    \end{corollary}

    \section{The geometric side of the trace formula}

    \begin{proposition}
        We have a two-to-one-map
        \[
            \curlybr[\big]{\text{$\Gamma$-conjugacy classes in $\Gamma \setminus \curlybr{\pm I}$}} \to \curlybr[\big]{\text{closed geodesics in $\Gamma \backslash \calH$}}
        \]
        that is given as follows.
        Every element $\gamma \in \Gamma \setminus \curlybr{\pm I}$ is hyperbolic, i.e.\ acts on $\calH$ by translating along a (uniquely determined) geodesic.
        The image of this geodesic then is a closed geodesic in $\Gamma \backslash \calH$.

        \begin{itemize}
            \item
            Write $\ell(\gamma)$ for the length of the closed geodesic corresponding to $\gamma$.
            Every $\gamma \in \Gamma \setminus \curlybr{\pm I}$ is $G$-conjugate to a unique element $\pm a(u)$ with $u \in \RR_{> 0}$ and we have $\ell(\gamma) = u$.

            \item
            The centralizer $G_{\gamma}$ is $G$-conjugate to $\pm A$ and we have $\Gamma_{\gamma} = \pm \gamma_0^{\ZZ}$ for a unique $\gamma_0 \in \Gamma$ with $\tr(\gamma_0) > 2$ and $\gamma = \pm \gamma_0^k$ with $k > 0$.

            \item
            We have $\vol(\Gamma_{\gamma} \backslash G_{\gamma}) = \ell(\gamma_0)$ and for $f \in C_c^{\infty}(G//K)$ we have
            \[
                \int_{G_{\gamma} \backslash G} f(g^{-1} \gamma g) \, \dd g = \frac{1}{2 \cdot \roundbr[\big]{e^{\ell(\gamma)/2} - e^{-\ell(\gamma)/2}}} \cdot g_f \roundbr[\big]{\ell(\gamma)} \, .
            \]

            \item
            We have $\vol(\Gamma \backslash G) = \frac{1}{2} \vol(\Gamma \backslash \calH)$ and for $f \in C_c^{\infty}(G//K)$ we have
            \[
                f(\pm I) = \frac{1}{2 \pi} \int_{0}^{\infty} \widehat{g_f}(u) u \tanh(\pi u) \, \dd u \, .
            \]
        \end{itemize}
    \end{proposition}

    \begin{theorem}[Trace formula]
        Let $0 = \lambda_0 < \lambda_1 \leq \lambda_2 \leq \dotsb$ be the negatives of the eigenvalues of $\Delta$ acting on $\calA(\Gamma \backslash \calH)$, with multiplicities, and write $\lambda_j = \frac{1}{4} + r_j^2$ with $r_j \in \RR_{\geq 0} \cup [0, \frac{1}{2}] i$.
        
        Then, for $g \in C_c^{\infty}(\RR)^{\even}$, we have
        \[
            \sum_{j = 0}^{\infty} \widehat{g}(r_j) = \frac{\vol(\Gamma \backslash \calH)}{2 \pi} \int_{0}^{\infty} \widehat{g}(u) u \tanh(\pi u) \, \dd u + \sum_{\gamma \in \curlybr{\Gamma}/\pm, \, \gamma \neq \pm I} \frac{\ell(\gamma_0)}{e^{\ell(\gamma)/2} - e^{- \ell(\gamma)/2}} \cdot g\roundbr[\big]{\ell(\gamma)} \, .
        \]
    \end{theorem}

    The trace formula in this form can be applied to give a proof of Weyl's law.

    \begin{theorem}[Weyl's law]
        For $T \in \RR_{\geq 0}$ we set $N_{\Gamma}(T) \coloneqq \# \set{j \geq 0}{\lambda_j < T}$.
        Then we have
        \[
            N_{\Gamma}(T) \sim \frac{\vol(\Gamma \backslash \calH)}{4 \pi} \cdot T
        \]
        for $T \to \infty$.
    \end{theorem}

    \printbibliography
\end{document}